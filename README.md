/start botu başlatır

/me senin hakkında bilgi verir

/uptime server uptime bilgisini gösterir

/echo yazdığını tekrar eder

/youtube youtube videolarını mp3e dönüştürüp gönderir

/date sunucu tarih saatini gösterir

/calc hesap makinası

/weather hava durumunu gösterir

/github github reposunu zip olarak indirip yollar

/sed sed ile regex yapar

/grep grep ile regex yapar

/man man-db (ingilizce)

/termbin gönderdiğiniz çıktıyı pasteler ve linkini yollar.

/trans çeviri yapar

/tts seslendirme yapar

/whois bir seta hakkında bilgi gösterir.

Bug bulursanız @parduscix'e haber verin

Kaynak kod: https://github.com/parduscix/paledega_bot
